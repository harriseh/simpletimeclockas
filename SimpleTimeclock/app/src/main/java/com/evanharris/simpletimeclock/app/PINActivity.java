package com.evanharris.simpletimeclock.app;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


public class PINActivity extends ActionBarActivity {
    private String pinNum;
    private boolean edited;
    public final static String PINSTRING_KEY = "com.evanharris.simpletimeclock.MESSAGE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin);

        final TextView txtPin = (TextView)findViewById(R.id.enterPin);
        txtPin.setText("Enter pin");
        edited = false;
        pinNum = "";
        final Button b0 = (Button)findViewById(R.id.b0);
        final Button b1 = (Button)findViewById(R.id.b1);
        final Button b2 = (Button)findViewById(R.id.b2);
        final Button b3 = (Button)findViewById(R.id.b3);
        final Button b4 = (Button)findViewById(R.id.b4);
        final Button b5 = (Button)findViewById(R.id.b5);
        final Button b6 = (Button)findViewById(R.id.b6);
        final Button b7 = (Button)findViewById(R.id.b7);
        final Button b8 = (Button)findViewById(R.id.b8);
        final Button b9 = (Button)findViewById(R.id.b9);
        final ImageButton bBack = (ImageButton)findViewById(R.id.bDel);
        final Button bDone = (Button)findViewById(R.id.bEnter);

        b0.setOnClickListener(new View.OnClickListener() { @Override public void onClick(View v) { manageKey("0"); }});
        b1.setOnClickListener(new View.OnClickListener() { @Override public void onClick(View v) { manageKey("1"); }});
        b2.setOnClickListener(new View.OnClickListener() { @Override public void onClick(View v) { manageKey("2"); }});
        b3.setOnClickListener(new View.OnClickListener() { @Override public void onClick(View v) { manageKey("3"); }});
        b4.setOnClickListener(new View.OnClickListener() { @Override public void onClick(View v) { manageKey("4"); }});
        b5.setOnClickListener(new View.OnClickListener() { @Override public void onClick(View v) { manageKey("5"); }});
        b6.setOnClickListener(new View.OnClickListener() { @Override public void onClick(View v) { manageKey("6"); }});
        b7.setOnClickListener(new View.OnClickListener() { @Override public void onClick(View v) { manageKey("7"); }});
        b8.setOnClickListener(new View.OnClickListener() { @Override public void onClick(View v) { manageKey("8"); }});
        b9.setOnClickListener(new View.OnClickListener() { @Override public void onClick(View v) { manageKey("9"); }});
        bBack.setOnClickListener(new View.OnClickListener() { @Override public void onClick(View v) { manageKey("BACK"); }});
        bDone.setOnClickListener(new View.OnClickListener() { @Override public void onClick(View v) { manageKey("DONE"); }});
    }


    private void manageKey(String key){
        final TextView txtPin = (TextView)findViewById(R.id.enterPin);

        if (!edited){
            txtPin.setText("");
            edited = true;
        }

        if (key == "BACK"){
            String str = txtPin.getText().toString().trim();
            String str2 = pinNum;
            if(str.length()!=0 && str2.length()!=0){
                str  = str.substring( 0, str.length() - 1 );
                str2  = str2.substring( 0, str2.length() - 1 );
                txtPin.setText ( str );
                pinNum = str2;
            }
        }
        else if (key == "DONE"){
            Intent intent = new Intent(this, PINResponseActivity.class);
            intent.putExtra(PINSTRING_KEY, pinNum);
            startActivity(intent);
        }
        else{
            if (pinNum.length() < 4){
                txtPin.append("*");
                String temp = pinNum;
                pinNum = temp.concat(key);
            }
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.pin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
