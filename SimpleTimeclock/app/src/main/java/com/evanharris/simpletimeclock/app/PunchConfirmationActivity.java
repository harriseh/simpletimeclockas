package com.evanharris.simpletimeclock.app;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.format.Time;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class PunchConfirmationActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_punch_confirmation);

        // Get the message from the intent
        Intent intent = getIntent();
        String empname = intent.getStringExtra(PINResponseActivity.NAME_KEY);
        String action = intent.getStringExtra(PINResponseActivity.ACTION_KEY);

        TextView toptext = (TextView) findViewById(R.id.userInfo);



        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String dateString = fmt.format(date);

        String t = empname + " " +  action +  " at " + dateString;

        toptext.setText(t);

        Button okB = (Button) findViewById(R.id.returnButton);
        okB.setOnClickListener(new View.OnClickListener() { @Override public void onClick(View v) { goHome(); }});

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.punch_confirmation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void goHome(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}
