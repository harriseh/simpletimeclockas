package com.evanharris.simpletimeclock.app;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends ActionBarActivity {
    private Timer myTimer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myTimer = new Timer(); // The clock
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() { TimerMethod(); }
        }, 0, 1000);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void TimerMethod()
    {	//This method is called directly by the timer and runs in the same thread as the timer. We call the method that will work with the UI through the runOnUiThread method.
        this.runOnUiThread(Timer_Tick);
    }


    private Runnable Timer_Tick = new Runnable() {
        public void run() { //This method runs in the same thread as the UI. Shows the clock in the activity
            TextView clock = (TextView)findViewById(R.id.Clock);
            Date d = new Date();
            CharSequence s  = DateFormat.format("hh:mm:ss ", d.getTime());
            clock.setText(s);
        }
    };

    public void initPinView(View view) {
        Intent intent = new Intent(this,PINActivity.class);
        startActivity(intent);
    }
}
