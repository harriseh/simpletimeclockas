package com.evanharris.simpletimeclock.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;


public class PINResponseActivity extends ActionBarActivity {


    //                  GLOBALS

    private ProgressDialog pDialog;

    // URL to get employees JSON
    private static String url = "http://137.22.238.248/simpletimeclock/employees.json";

    // Where to POST the employees time punch data
    private static String desturl = "http://137.22.238.248/simpletimeclock/recvpunch.php";

    // JSON Node names
    private static final String TAG_EMPLOYEES = "employees";
    private static final String TAG_ID = "id";
    private static final String TAG_PIN = "pin";
    private static final String TAG_NAME = "name";
    private static final String TAG_POSITION = "position";
    public final static String NAME_KEY = "com.evanharris.simpletimeclock.NAME_KEY";
    public final static String ACTION_KEY = "com.evanharris.simpletimeclock.ACTION_KEY";

    //
    JSONArray employees = null;
    //set to true if a matching pin is found in employees JSON
    private boolean pinFound = false;
    //
    private String pinReceived = "";
    private String found_name = "";
    private String found_position = "";
    private String found_id = "";
    private String action = "";





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pinresponse);

        //get the pin number from PINActivity
        Intent intent = getIntent();
        pinReceived = intent.getStringExtra(PINActivity.PINSTRING_KEY);

        pinFound = false;
        //get the list of employees
        new GetEmployees().execute();

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.pinresponse, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    /*
    Private class used to get the list of employees to check for a matching pin
     */
    private class GetEmployees extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(PINResponseActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            ServiceHandler sh = new ServiceHandler();
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.GET);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    employees = jsonObj.getJSONArray(TAG_EMPLOYEES);
                    for (int i = 0; i < employees.length(); i++) {
                        JSONObject c = employees.getJSONObject(i);
                        String pinNum = c.getString(TAG_PIN);
                        if (pinNum.equalsIgnoreCase(pinReceived)){
                            found_id = c.getString(TAG_ID);
                            found_name = c.getString(TAG_NAME);
                            found_position = c.getString(TAG_POSITION);
                            pinFound = true;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();

            TableLayout tt = (TableLayout) findViewById(R.id.mTable);
            TableLayout bt = (TableLayout) findViewById(R.id.bottomTable);
            TextView errormsg = (TextView) findViewById(R.id.invalidText);
            Button okB = (Button) findViewById(R.id.returnButton);

            if (pinFound){
                tt.setVisibility(View.VISIBLE);
                bt.setVisibility(View.VISIBLE);
                errormsg.setVisibility(View.INVISIBLE);
                okB.setVisibility(View.INVISIBLE);

                TextView name = (TextView) findViewById(R.id.empName2);
                name.setText(found_name);

                TextView pos = (TextView) findViewById(R.id.empPos2);
                pos.setText(found_position);

                TextView empId = (TextView) findViewById(R.id.empId2);
                empId.setText(found_id);

                Button clockin = (Button) findViewById(R.id.clockIN);
                clockin.setOnClickListener(new View.OnClickListener() { @Override public void onClick(View v) { clockIN(); }});

                Button clockout = (Button) findViewById(R.id.clockOUT);
                clockout.setOnClickListener(new View.OnClickListener() { @Override public void onClick(View v) { clockOUT(); }});
            }
            else{
                tt.setVisibility(View.INVISIBLE);
                bt.setVisibility(View.INVISIBLE);
                errormsg.setVisibility(View.VISIBLE);
                okB.setVisibility(View.VISIBLE);
                okB.setOnClickListener(new View.OnClickListener() { @Override public void onClick(View v) { goHome(); }});
            }
        }
    }

    public void clockIN(){
        action = "clocked in";
        CommitPunch cp = new CommitPunch();
        cp.execute();
    }

    public void clockOUT(){
        action = "clocked out";
        CommitPunch cp = new CommitPunch();
        cp.execute();
    }
    private class CommitPunch extends AsyncTask<String,Integer,Boolean> {

        protected Boolean doInBackground(String... params) {
            boolean result = true;
            //get current time for logging
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date();
            String dateString = fmt.format(date);
            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost del = new HttpPost(desturl);
                del.setHeader("Accept", "application/json");
                del.setHeader("Content-type", "application/json");

                String jsonText = "";
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("name", found_name);
                    jsonObject.put("position", found_position);
                    jsonObject.put(action, dateString);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                jsonText = jsonObject.toString();
                StringEntity se = new StringEntity(jsonText);
                del.setEntity(se);

                ResponseHandler responseHandler = new BasicResponseHandler();
                HttpResponse httpResponse =  httpClient.execute(del);

            } catch (Exception e) {

            }
            return result;
        }

        protected void onPostExecute(Boolean result) {
            Intent intent = new Intent(PINResponseActivity.this, PunchConfirmationActivity.class);
            intent.putExtra(ACTION_KEY,action);
            intent.putExtra(NAME_KEY, found_name);
            startActivity(intent);
        }

    }

    public void goHome(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


}
